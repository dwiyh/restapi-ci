<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Example extends REST_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('UsersModel');
    }

    public function users_get()
    {
        $id = $this->get('id');
        if ($id === NULL) {
            $users = $this->UsersModel->all_users();
            $this->response($users, REST_Controller::HTTP_OK);
        } else {

            $users = $this->UsersModel->users_by_id($id);
            $this->set_response($users, REST_Controller::HTTP_OK);
        }
    }

    public function users_post()
    {
        $response = $this->UsersModel->add_users(
            $this->post('nama'),
            $this->post('alamat')
        );
        $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    public function users_put()
    {
        $response = $this->UsersModel->update_users(
            $this->put('id'),
            $this->put('nama'),
            $this->put('alamat')
        );
        $this->set_response($response, REST_Controller::HTTP_CREATED);
    }

    public function users_delete()
    {
        $response = $this->UsersModel->delete_users(
            $this->delete('id')
        );
        $this->set_response($response, REST_Controller::HTTP_OK);
    }
}
