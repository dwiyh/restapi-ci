<?php
class UsersModel extends CI_Model
{
    public function empty_response()
    {
        $response['status'] = 502;
        $response['error'] = true;
        $response['message'] = 'Field tidak boleh kosong';
        return $response;
    }
    public function add_users($name, $address)
    {
        if (empty($name) || empty($address) ) {
            return $this->empty_response();
        } else {
            $data = array(
                "nama" => $name,
                "alamat" => $address,
            );
            $insert = $this->db->insert("user", $data);
            if ($insert) {
                $response['status'] = 200;
                $response['error'] = false;
                $response['message'] = 'Data user ditambahkan.';
                return $response;
            } else {
                $response['status'] = 502;
                $response['error'] = true;
                $response['message'] = 'Data user gagal ditambahkan.';
                return $response;
            }
        }
    }
    public function all_users()
    {
        $all = $this->db->get("user")->result();
        $response['status'] = 200;
        $response['error'] = false;
        $response['users'] = $all;
        return $response;
    }

    public function users_by_id($id)
    {
        $this->db->where('id',$id);
		$result = $this->db->get('user')->result();
        $response['status'] = 200;
        $response['error'] = false;
        $response['users'] = $result;
        return $response;
    }

    public function delete_users($id)
    {
        if ($id == '') {
            return $this->empty_response();
        } else {
            $where = array(
                "id" => $id
            );
            $this->db->where($where);
            $delete = $this->db->delete("user");
            if ($delete) {
                $response['status'] = 200;
                $response['error'] = false;
                $response['message'] = 'Data users dihapus.';
                return $response;
            } else {
                $response['status'] = 502;
                $response['error'] = true;
                $response['message'] = 'Data users gagal dihapus.';
                return $response;
            }
        }
    }

    public function update_users($id, $name, $address)
    {
        if ($id == '' || empty($name) || empty($address)) {
            return $this->empty_response();
        } else {
            $where = array(
                "id" => $id
            );
            $set = array(
                "nama" => $name,
                "alamat" => $address,
            );
            $this->db->where($where);
            $update = $this->db->update("user", $set);
            if ($update) {
                $response['status'] = 200;
                $response['error'] = false;
                $response['message'] = 'Data users diubah.';
                return $response;
            } else {
                $response['status'] = 502;
                $response['error'] = true;
                $response['message'] = 'Data users gagal diubah.';
                return $response;
            }
        }
    }
}
